class Contact < ApplicationRecord
  belongs_to :user
  validates :name, presence: { message: 'needs a name!' }, uniqueness: { scope: :user_id }
  has_many :phones
end
