class Phone < ApplicationRecord
  belongs_to :contact
  validates :number, presence: { message: 'needs a number!' }, uniqueness: { scope: :contact_id }
  validates :category, inclusion: { in: ['home', 'work', 'other'] }, presence: { message: "category must be 'home', 'work', or 'other'!" }
  validates :main, inclusion: [true, false]
  validates :main, uniqueness: { scope: :contact_id }, if: :main

  def save
    if self.main
      cont_id = self.contact_id
      contact = Contact.all.find_by_id(cont_id)
      registered_phones = contact.phones
      if registered_phones.map {|phone| phone.main}.any?
        registered_phones.each do |phone|
          phone.main = false
          phone.save
        end
      end
    end
    super
  end

end
