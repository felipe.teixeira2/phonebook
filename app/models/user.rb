class User < ApplicationRecord
  validates :name, presence: { message: 'must be provided.' }, uniqueness: true
  has_many :contacts
  has_many :phones, through: :contacts

  def phone_count
    phones.count
  end
end
