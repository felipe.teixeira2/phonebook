require 'rails_helper'

RSpec.describe Contact, type: :model do
  fixtures :users, :contacts

  describe 'database' do
    it 'contains Paul' do
      expect(contacts(:paul)).to be_valid
      #puts "I'm gonna print Paul: "
      #p contacts(:paul).user
    end
    # it 'does not contain' do
    #   expect(contacts(:sting)).not_to be_valid
    # end
  end

  describe 'validation' do
    describe 'name' do
      it 'must exist' do
        expect(contacts(:paul)).to be_valid
      end
      it 'must not be blank' do
        expect(contacts(:fan)).not_to be_valid
      end
    end
    describe 'birth' do
      it 'may be present' do
        expect(contacts(:paul)).to be_valid
      end
      it 'may not be present' do
        expect(contacts(:terminator)).to be_valid
      end
    end
    describe 'user' do
      it 'must be present' do
        expect(contacts(:paul)).to be_valid
      end
#      it 'must not be blank' do
#        expect(contacts(:newton)).not_to be_valid
#      end
    end
  end

end
