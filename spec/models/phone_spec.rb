require 'rails_helper'

RSpec.describe Phone, type: :model do
  fixtures :users, :contacts, :phones

  describe 'validation' do
    describe 'number' do
      it 'must exist' do
        expect(phones(:withphone)).to be_valid
      end
      it 'must not be blank' do
        expect(phones(:withoutphone)).not_to be_valid
      end
      it 'must be unique' do
        expect(phones(:repeated)).not_to be_valid
      end
    end

    describe 'category' do
      it "can be 'home'" do
        expect(phones(:home)).to be_valid
      end

      it "can be 'work'" do
        expect(phones(:work)).to be_valid
      end

      it "can be 'other'" do
        expect(phones(:other)).to be_valid
      end

      it "can't be be anything else" do
        expect(phones(:badcat)).not_to be_valid
      end
    end

    describe 'main' do
      it 'must exist' do
        expect(contacts(:paul)).to be_valid
      end

      it "can't be assigned to more than one number" do
        expect(contacts(:fan)).not_to be_valid
      end

      let(:autoreassign) { Phone.new number:666, category:'home', main: true, contact:contacts(:paul) }
      it "should think new conflicting main is forbidden..." do
        expect(autoreassign).not_to be_valid
      end
      it "...but finds that main can be automatically reassigned" do
        autoreassign.save
        expect(autoreassign).to be_valid
      end
    end
  end

end
