require 'rails_helper'

RSpec.describe User, type: :model do

  fixtures :users

  describe 'validations' do

    describe 'user' do
      it 'must have a name' do
        expect(users(:nameless)).not_to be_valid
        # expect(users(:jane)).to be_valid
      end
      it 'the name must be unique' do
        expect(users(:jack)).not_to be_valid
        # expect(users(:jane)).to be_valid
      end
      it 'may or may not have an age' do
        expect(users(:ageless)).to be_valid
        # expect(users(:jane)).to be_valid
      end
      it 'may or may not have a biography' do
        expect(users(:bioless)).to be_valid
        # expect(users(:jane)).to be_valid
      end
    end
  end

  describe 'database' do
    it 'has six people' do
      expect(users.count).to eq(6)
    end
  end

end
